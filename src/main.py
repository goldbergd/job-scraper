#!/usr/bin/python3
import argparse
import logging

import companies
import job_reports
import log

if __name__ == '__main__':
    parser = argparse.ArgumentParser('Chceks for new jobs!')
    parser.add_argument('--verbose', '-v', action='count', default=0)
    parser.add_argument('--query', '-q', action='store_true')
    parser.add_argument('--force', '-f', action='store_true')
    parser.add_argument('--debug', '-d', action='store_true',
                        help='This will cause the email not to be sent. Just print what would be sent')
    parser.add_argument('--email', '-e', nargs='+',
                        default='dgoldberg92@gmail.com')
    args = parser.parse_args()
    log.handle_logging_file('logs', 'job_checker.log', args.verbose)
    if args.query:
        job_reports.print_db()
    else:
        c = job_reports.job_check(companies.all_companies)
        logger = logging.getLogger('main')
        logger.info(f'Latest info {c}')
        job_reports.save_and_email_if_needed(c, args.email, args.force, args.debug)
