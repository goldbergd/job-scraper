import logging
import logging.handlers
import os
import sys

TRACE = 5


def _pad_end(lst, sz):
    while lst and len(lst) < sz:
        lst.append(lst[-1])


def handle_logging(verbose, formatter_str='%(name)s: %(message)s'):
    logging.addLevelName(TRACE, 'TRACE')
    stdout_handler = logging.StreamHandler(sys.stdout)
    log = logging.getLogger()
    log.setLevel(logging.CRITICAL)
    if verbose == 1:
        stdout_handler.setLevel(logging.ERROR)
        log.setLevel(logging.ERROR)
        print('Write level: ERROR')
    elif verbose == 2:
        stdout_handler.setLevel(logging.WARNING)
        log.setLevel(logging.WARNING)
        print('Write level: WARNING')
    elif verbose == 3:
        stdout_handler.setLevel(logging.INFO)
        log.setLevel(logging.INFO)
        print('Write level: INFO')
    elif verbose == 4:
        stdout_handler.setLevel(logging.DEBUG)
        log.setLevel(logging.DEBUG)
        print('Write level: DEBUG')
    elif verbose > 4:
        stdout_handler.setLevel(TRACE)
        log.setLevel(TRACE)
        print('Write level: TRACE')
    else:
        stdout_handler.setLevel(logging.CRITICAL)
    stdout_format = logging.Formatter(formatter_str)
    stdout_handler.setFormatter(stdout_format)
    log.addHandler(stdout_handler)


def handle_logging_file(logging_dir, name, verbose=0, formatter_str='%(name)s: %(message)s', levels=[logging.DEBUG], backup_counts=[1], max_bytes=[5*1024*1024]):
    handle_logging(verbose, formatter_str)
    try:
        os.mkdir(logging_dir)
    except FileExistsError as e:
        pass
    log_file_path = os.path.join(logging_dir, name)
    logging_format = logging.Formatter(
        "%(asctime)-15s %(levelname)s %(name)s - %(message)s")
    log = logging.getLogger()
    _pad_end(backup_counts, len(levels))
    _pad_end(max_bytes, len(levels))
    for level, count, max_byte in zip(levels, backup_counts, max_bytes):
        full_log_name = log_file_path
        if level < max(levels):
            full_log_name = log_file_path[:-4] + \
                '_' + logging.getLevelName(level) + '.log'
        logging_handler = logging.handlers.RotatingFileHandler(
            full_log_name, mode='a', maxBytes=max_byte, backupCount=count, encoding=None, delay=0)
        logging_handler.setLevel(level)
        logging_handler.setFormatter(logging_format)
        log.addHandler(logging_handler)
    if log.getEffectiveLevel() > min(levels):
        log.setLevel(min(levels))
