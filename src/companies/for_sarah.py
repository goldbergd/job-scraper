import logging
from urllib.parse import urljoin

# This uses beautiful because of self._soup
from bs4 import BeautifulSoup
from company import Company
from job import Job


class DcGreens(Company):
    def __init__(self) -> None:
        super().__init__('DC Greens',
                         'https://www.dcgreens.org/employment-1',
                         ['sarah.flinspach@gmail.com'])

    def check(self) -> set:
        jobs = set()
        log = logging.getLogger('DcGreens.check')
        soup = self._soup.find(class_='Main-content').extract()
        for c in soup.find_all_next('a'):
            j = Job()
            j.position_title = c.string
            j.url = urljoin(self.url, c['href'])
            jobs.add(j)
            log.info(j.__dict__)
        return jobs

# Info: Maybe use https://pypi.org/project/phantomjs/ for parsing JS jobs
# https://stackoverflow.com/questions/8049520/web-scraping-javascript-page-with-python


class FoodAndFriends(Company):
    def __init__(self) -> None:
        super().__init__('Food & Friends',
                         'https://foodandfriends.org/employment/',
                         ['sarah.flinspach@gmail.com'])

    def check(self) -> set:
        jobs = set()
        log = logging.getLogger('FoodAndFriends.check')
        marker = self._soup.find('h2', string='Employment')
        for m in marker.find_all_next('a'):
            j = Job()
            j.position_title = m.string
            # j.description = child.div.string
            j.location = "219 Riggs Road NE Washington, DC 20011"
            j.url = m['href']
            j.location = "2401 Virginia Ave, NW Washington, DC 20037"
            if '/employment/' in j.url:
                jobs.add(j)
                log.info(j.__dict__)
            else:
                break
        return jobs


class MiriamsKitchen(Company):
    def __init__(self) -> None:
        super().__init__("Miriam's Kitchen",
                         'https://www.miriamskitchen.org/about/careers/',
                         ['sarah.flinspach@gmail.com'])

    def check(self) -> set:
        jobs = set()
        log = logging.getLogger('MiriamsKitchen.check')
        marker = self._soup.find('div', id='Open_Positions').extract()
        for m in marker.find_all_next('a'):
            j = Job()
            j.position_title = m.string
            j.location = "2401 Virginia Ave, NW Washington, DC 20037"
            j.url = m['href']
            log.info(j.__dict__)
            jobs.add(j)
        return jobs


all_companies = set()
all_companies.add(DcGreens())
all_companies.add(FoodAndFriends())
all_companies.add(MiriamsKitchen())
