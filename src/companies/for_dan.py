import logging
from urllib.parse import urljoin

# This uses beautiful because of self._soup
from bs4 import BeautifulSoup
from company import Company
from job import Job


class DevlopmentSeed(Company):
    def __init__(self) -> None:
        super().__init__('Development Seed',
                         'https://developmentseed.org/careers',
                         ['dgoldberg92@gmail.com'])

    def check(self) -> set:
        jobs = set()
        log = logging.getLogger('DevlopmentSeed.check')
        for child in self._soup.find(class_='_section-positions__PositionsList-sc-1ohfpu7-3 dBHSLI').children:
            j = Job()
            j.position_title = child.a.string
            j.description = child.div.string
            j.location = child.p.string
            j.url = urljoin(self.url, child.footer.a['href'])
            jobs.add(j)
            log.debug(j.__dict__)
        return jobs


all_companies = set()
all_companies.add(DevlopmentSeed())
