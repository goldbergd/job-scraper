from companies import for_dan, for_sarah

all_companies = set()
all_companies.update(for_dan.all_companies)
all_companies.update(for_sarah.all_companies)
