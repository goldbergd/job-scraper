import json
import logging
import os
import sqlite3

import email_helper
from company import Company
from job import Job


def job_check(companies):
    jobs = {}
    for c in companies:
        jobs[c] = c.check()
    return jobs


def _create_table(cur):
    cur.execute('SELECT name FROM sqlite_master ;')
    found = False
    for table_name in cur.fetchall():
        found = table_name[0] == 'jobs'
        if found:
            break
    if not found:
        cur.execute(
            'create table jobs(date TEXT,name TEXT,company TEXT,jobs TEXT) ;')


def j_from_db(jobs_json):
    jobs = set()
    for job in json.loads(jobs_json):
        j = Job()
        j.__dict__ = job
        jobs.add(j)
    return jobs


def j_to_json(jobs):
    js = [j.__dict__ for j in sorted(jobs, key=lambda x: x.position_title)]
    return json.dumps(js)


def to_dict(rows):
    log = logging.getLogger('to_dict')
    jobs = {}
    for r in rows:
        company = Company('', '')
        company.from_json(r[2])
        jobs[company] = j_from_db(r[3])
        log.debug(f'{company}: {jobs[company]}')
    return jobs


def addremovechange_in_db(prev_jobs, cur_jobs, cursor) -> dict:
    changed = {}
    log = logging.getLogger('addremovechange_in_db')
    prev_names = set([c.name for c in prev_jobs])
    cur_names = set([c.name for c in cur_jobs])
    update_names = set()
    for c in prev_jobs:
        for cc in cur_jobs:
            if c.name == cc.name:
                log.debug(f'Compare new and old: {c.name}')
                old_json = j_to_json(prev_jobs[c])
                new_json = j_to_json(cur_jobs[cc])
                if new_json != old_json:
                    log.debug(f'Updated job found for: {c.name}')
                    log.debug(old_json)
                    log.debug(new_json)
                    update_names.add(c.name)
                if not c.is_same(cc):
                    log.debug(f'Updated compnay for: {c.name}')
                    log.debug(c)
                    log.debug(cc)
                    update_names.add(c.name)
                break
    for name in prev_names:
        log.debug(
            f'Checking {name} not in {cur_names} or {name} in {update_names}')
        if ((name not in cur_names)
                or (name in update_names)):
            log.info(f'Removing {name}')
            cursor.execute('DELETE FROM jobs WHERE name = ? ;', (name,))
            # TODO: Email a removed message

    for company in cur_jobs:
        if ((company.name not in prev_names)
                or company.name in update_names):
            log.info(f'Adding {company.name}')
            cursor.execute('INSERT INTO jobs(date,name,company,jobs) VALUES (?,?,?,?) ;', (
                company.date, company.name, company.to_json(), j_to_json(cur_jobs[company])))
            changed[company.name] = True
    return changed


def save_and_email_if_needed(jobs, email_to, force=False, debug=False):
    log = logging.getLogger('save_and_email_if_needed')
    try:
        os.mkdir('db')
    except FileExistsError as e:
        log.debug('Tried to make directory "db" but it already exists')
        log.debug(e)
    con = sqlite3.connect('db/job.db')
    cur = con.cursor()
    _create_table(cur)
    cur.execute('SELECT * FROM jobs ;')
    prev_jobs = to_dict(cur.fetchall())
    changed = addremovechange_in_db(prev_jobs, jobs, cur)
    con.commit()
    cur.close()
    con.close()
    if changed or force:
        email_helper.email(changed, jobs, email_to, debug)


def print_db():
    con = sqlite3.connect('db/job.db')
    cur = con.cursor()
    _create_table(cur)
    cur.execute('SELECT * FROM jobs ;')
    prev_jobs = to_dict(cur.fetchall())
    print(prev_jobs)
    cur.close()
    con.close()
