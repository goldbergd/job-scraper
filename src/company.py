import datetime
import json
import logging

import requests
from bs4 import BeautifulSoup


class Company(object):
    def __init__(self, name, url, send_to=[]) -> None:
        super().__init__()
        self.url = url
        self._soup = BeautifulSoup(requests.get(url).text, 'html.parser') if url else None
        self.name = name
        self.date = datetime.datetime.now()
        self.send_to = send_to

    def check(self):
        pass

    def __str__(self) -> str:
        return f'[{self.date}]{self.name}'

    def __repr__(self) -> str:
        return self.__str__()

    def pretty(self) -> str:
        return f"""<u><b><a href="{self.url}">{self.name}</a></b></u> ({datetime.datetime.strftime(self.date, '%d%b')})"""

    def to_json(self) -> str:
        copy = self.__dict__.copy()
        del copy['_soup']
        copy['date'] = str(self.date)
        return json.dumps(copy)

    def from_json(self, j):
        log = logging.getLogger('Company.from_json')
        d = json.loads(j)
        self.__dict__ = d.copy()
        self.date = datetime.datetime.fromisoformat(d['date'])
        log.info(f'{self}: {d}')

    def is_same(self, other) -> bool:
        return (self.url == other.url
                and self.name == other.name
                and self.send_to == other.send_to)
