class Job(object):
    def __init__(self):
        super().__init__()
        self.position_title = 'Untitled'
        self.description = 'No Description'
        self.salary = '$$$?'
        self.url = ''
        self.location = 'Unknown Location'

    def __str__(self) -> str:
        return self.position_title

    def __repr__(self) -> str:
        return self.__str__()

    def pretty(self) -> str:
        return f"""&ensp;<a href="{self.url}">{self.position_title}</a> [{self.salary}] - {self.location}:
<br>&emsp;{self.description}</p>"""
