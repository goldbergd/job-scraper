import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import lxml
import lxml.html
from dotenv import load_dotenv

import company  # calls .pretty()
import job  # calls .pretty()
import oauth2

load_dotenv()

GOOGLE_ACCOUNT = os.environ['GOOGLE_ACCOUNT']
GOOGLE_CLIENT_ID = os.environ['GOOGLE_CLIENT_ID']
GOOGLE_REFRESH_TOKEN = os.environ['GOOGLE_REFRESH_TOKEN']
GOOGLE_CLIENT_SECRET = os.environ['GOOGLE_CLIENT_SECRET']


def send_mail(toaddr, subject, message):
    response = oauth2.RefreshToken(GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET, GOOGLE_REFRESH_TOKEN)
    auth_string = oauth2.GenerateOAuth2String(GOOGLE_ACCOUNT, response['access_token'])
    msg = MIMEMultipart('related')
    msg['Subject'] = subject
    msg['From'] = GOOGLE_ACCOUNT
    msg['To'] = toaddr if isinstance(toaddr, str) else ', '.join(toaddr)
    msg.preamble = 'This is a multi-part message in MIME format.'
    msg_alternative = MIMEMultipart('alternative')
    msg.attach(msg_alternative)
    part_text = MIMEText(lxml.html.fromstring(message).text_content().encode('utf-8'), 'plain', _charset='utf-8')
    part_html = MIMEText(message.encode('utf-8'), 'html', _charset='utf-8')
    msg_alternative.attach(part_text)
    msg_alternative.attach(part_html)

    # smtp_conn = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    smtp_conn = smtplib.SMTP('smtp.gmail.com', 587)
    # smtp_conn.set_debuglevel(True)
    smtp_conn.ehlo('test')
    smtp_conn.starttls()
    smtp_conn.docmd('AUTH', f'XOAUTH2 {auth_string.decode()}')

    smtp_conn.sendmail(GOOGLE_ACCOUNT, toaddr if isinstance(toaddr, str) else ', '.join(toaddr), msg.as_string())
    smtp_conn.quit()


def email(changed_jobs, jobs, default_send_to, debug=False):
    email_strs = {}
    for company in sorted(jobs, key=lambda x: x.name):
        if not changed_jobs or (company.name in changed_jobs and changed_jobs[company.name]):
            c_emails = ','.join(company.send_to)
            if c_emails not in email_strs:
                email_strs[c_emails] = []
            email_strs[c_emails].append(company.pretty())
            for job in sorted(jobs[company], key=lambda x: x.position_title):
                email_strs[c_emails].append(job.pretty())
    for send_to in email_strs:
        final_send_to = send_to
        if not send_to:
            final_send_to = default_send_to
        if debug:
            print(f'TO: {final_send_to}')
            print('<br>'.join(email_strs[send_to]))
        else:
            send_mail(final_send_to,
                      'Yum yummy jobs email', '<br>'.join(email_strs[send_to]))
