Requires:
 * Python3
 * python-dotenv
    - pip install python-dotenv
 * BeautifulSoup
    - pip install beautifulsoup4
 * Secret client key for DGoldberg92@gmail.com
 * ".env" (see below)

.env:
See https://developers.google.com/identity/protocols/oauth2 for help. Contact me if you want to jump on my gmail project. Get your client id and client secret from https://console.developers.google.com/
GOOGLE_ACCOUNT="[gmail_username]@gmail.com"
GOOGLE_CLIENT_ID="[your specfic info].apps.googleusercontent.com"
GOOGLE_CLIENT_SECRET=""
GOOGLE_REFRESH_TOKEN="[Obtained by running _oauth2.py_]"